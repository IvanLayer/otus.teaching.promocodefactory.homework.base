﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        public Task<T> Create(T newData);

        public Task<T> Update(T newData);

        public Task<T> Delete(Guid id);

    }
}