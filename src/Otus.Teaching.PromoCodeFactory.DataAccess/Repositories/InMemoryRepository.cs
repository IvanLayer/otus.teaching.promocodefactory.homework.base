﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }


        public Task<T> Create(T newData)
        {
            Data = Data.Append(newData);

            return Task.FromResult(newData);
        }

        public Task<T> Update(T newData)
        {
            //Task<T> ObjectForUpdate = GetByIdAsync(newData.Id);
            //ObjectForUpdate = Task.FromResult(newData);

            //return ObjectForUpdate;
            //IEnumerable<T> nData = new IEnumerable<T>;
            List<T> nData = new List<T>();

            foreach (T d in Data)
            {
                if (d.Id == newData.Id)
                {
                    nData.Add(newData);
                }
                else
                {
                    nData.Add(d);
                }
            }
            Data = nData;

            return Task.FromResult(newData);
        }

        public Task<T> Delete(Guid id)
        {
            List<T> nData = new List<T>();

            foreach (T d in Data)
            {
                if (d.Id != id)
                {
                    nData.Add(d);
                }
            }
            
            Data = nData;

            return Task.FromResult(Data.First());

        } // public bool Delete(Guid id)

    }
}