﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
            new EmployeeShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FullName = x.FullName,
            }).ToList();

            return employeesModelList;
        }


        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        //Create 
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> CreateEmployee(EmployeeRequest NewEmployee)
        {
            if (NewEmployee == null)
            {
                return NotFound();
            }

            // find\convert Roles
            var allSystemRoles = await _rolesRepository.GetAllAsync();

            List<Role> employeeRoles = new List<Role>();

            //PartnerManager, Admin
            foreach (string role in NewEmployee.Roles)
            {
                foreach (Role sysRole in allSystemRoles)
                {
                    if (sysRole.Name == role)
                    {
                        employeeRoles.Add(sysRole);
                    }
                }
            }

            if (employeeRoles.Count == 0)
            {
                return NotFound();
            }

            // fill new employee
            Employee employee = new Employee()
            {
                Id = Guid.NewGuid(),
                FirstName = NewEmployee.FirstName,
                LastName = NewEmployee.LastName,
                Email = NewEmployee.Email,
                AppliedPromocodesCount = 0,
                Roles = employeeRoles
            };

            await _employeeRepository.Create(employee);

            // Response
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;

        }

        //Update
        [HttpPut]
        public async Task<ActionResult<Employee>> UpdateEmployee(Employee updateEmployee)
        {
            var result = await _employeeRepository.Update(updateEmployee);
 
            return result;
        }


        [HttpDelete]
        public async Task<ActionResult<Employee>> DeleteEmployee(Guid id)
        {
            var result = await _employeeRepository.Delete(id);

            return result;
        }


    }
}